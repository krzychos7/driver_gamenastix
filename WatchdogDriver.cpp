// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "WatchdogDriver.h"


vr::EVRInitError WatchdogDriver::Init(vr::IVRDriverContext* driverContext)
{
    return vr::EVRInitError::VRInitError_None;
}

void WatchdogDriver::Cleanup()
{
}
