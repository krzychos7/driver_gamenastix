// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once


/*
We wont be using any cryptography, DDE, RPC, etc.. so exclude these from the build process, not necessary but speeds up the build a bit
*/
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <chrono>
#include <thread>
#include <vector>

#include "ServerDriver.h"
#include "WatchdogDriver.h"

#include <openvr_driver.h>

#define HMD_DLL_EXPORT extern "C" __declspec(dllexport)

extern ServerDriver server_driver;
extern WatchdogDriver watchdog_driver;
