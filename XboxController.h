// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#ifndef _XBOX_CONTROLLER_H_
#define _XBOX_CONTROLLER_H_
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN // We don't want the extra stuff like MFC and such
#include <windows.h>
#include <XInput.h>     // XInput API
#pragma comment(lib, "XInput.lib")   // Library. If your compiler doesn't support this type of lib include change to the corresponding one

class XboxController
{
private:
	XINPUT_STATE _controllerState;
	int _controllerNum;
public:
	XboxController(int playerNumber);
	XINPUT_STATE GetState();
	bool IsConnected();
	void Vibrate(int leftVal = 0, int rightVal = 0);
};

#endif