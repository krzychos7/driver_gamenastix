// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#include "ServerDriver.h"
#include <cmath>

vr::EVRInitError ServerDriver::Init(vr::IVRDriverContext* driverContext)
{
    VR_INIT_SERVER_DRIVER_CONTEXT(driverContext);

    vr::DriverPose_t testPose = {0};
    testPose.deviceIsConnected = true;
    testPose.poseIsValid = true;
    testPose.willDriftInYaw = false;
    testPose.shouldApplyHeadModel = false;
    testPose.poseTimeOffset = 0;
    testPose.result = vr::ETrackingResult::TrackingResult_Running_OK;
    testPose.qDriverFromHeadRotation = {1, 0, 0, 0};
    testPose.qWorldFromDriverRotation = {1, 0, 0, 0};

    vr::VRControllerState_t testState;
    testState.ulButtonPressed = testState.ulButtonTouched = 0;

    trackerFootLeft = DiscoTracker("disco_tracker_foot_left", testPose, testState);
    trackerFootRight = DiscoTracker("disco_tracker_foot_right", testPose, testState);

    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_left", vr::TrackedDeviceClass_GenericTracker, &trackerFootLeft);
    vr::VRServerDriverHost()->TrackedDeviceAdded("disco_tracker_foot_right", vr::TrackedDeviceClass_GenericTracker, &trackerFootRight);

    return vr::EVRInitError::VRInitError_None;
}

void ServerDriver::Cleanup()
{
    // TODO
}

const char* const* ServerDriver::GetInterfaceVersions()
{
    return vr::k_InterfaceVersions;
}

void ServerDriver::RunFrame()
{
	static auto Gamepad = new XboxController(1);

    static double angle = 0;
    angle += 0.01;

    static std::chrono::milliseconds lastMillis = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds deltaTime = now - lastMillis;
    lastMillis = now;
	{
		vr::DriverPose_t leftPose = trackerFootLeft.GetPose();
		vr::DriverPose_t leftPrevious = leftPose;

		leftPose.vecPosition[0] = -0.2;
		leftPose.vecPosition[2] = (double)Gamepad->GetState().Gamepad.sThumbLX / 30000.0;
		leftPose.vecPosition[1] = (double)Gamepad->GetState().Gamepad.sThumbLY / 30000.0;

		trackerFootLeft.UpdateControllerPose(leftPose);
		vr::VRServerDriverHost()->TrackedDevicePoseUpdated(trackerFootLeft.getObjectID(), trackerFootLeft.GetPose(), sizeof(vr::DriverPose_t));
	}
	{
		vr::DriverPose_t rightPose = trackerFootRight.GetPose();
		vr::DriverPose_t rightPrevious = rightPose;

		rightPose.vecPosition[0] = 0.2;
		rightPose.vecPosition[2] = (double)Gamepad->GetState().Gamepad.sThumbRX / 30000.0;
		rightPose.vecPosition[1] = (double)Gamepad->GetState().Gamepad.sThumbRY / 30000.0;

		trackerFootRight.UpdateControllerPose(rightPose);
		vr::VRServerDriverHost()->TrackedDevicePoseUpdated(trackerFootRight.getObjectID(), trackerFootRight.GetPose(), sizeof(vr::DriverPose_t));
	}
}

bool ServerDriver::ShouldBlockStandbyMode()
{
    return false;
}

void ServerDriver::EnterStandby()
{
}

void ServerDriver::LeaveStandby()
{
}