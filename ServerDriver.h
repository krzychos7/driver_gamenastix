// This file is a part of Treadmill project.
// Copyright 2018 Disco WTMH S.A.

#pragma once

#include "DiscoTracker.h"
#include "XboxController.h"
#include <openvr_driver.h>
#include <algorithm>
#include <chrono>


class ServerDriver : public vr::IServerTrackedDeviceProvider
{
public:
    ServerDriver() = default;

    ~ServerDriver() = default;

    vr::EVRInitError Init(vr::IVRDriverContext *driverContext) override;

    void Cleanup() override;

    const char *const *GetInterfaceVersions() override;

    void RunFrame() override;

    bool ShouldBlockStandbyMode() override;

    void EnterStandby() override;

    void LeaveStandby() override;

private:
    DiscoTracker trackerFootLeft;
    DiscoTracker trackerFootRight;
};
